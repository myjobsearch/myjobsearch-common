use serde::{Deserialize, Serialize};

use crate::{utils::new_id, Identifiable, Social};

/// Structure to group data on a person:
///
///  * Phone number
///  * Email
///  * Note
///  * Social (linkedin, indeed...)
/// ### Example
///
/// ```rust, ignore
/// let data = PersonData::new()
///     .set_phone("0102030405".to_string())
///     .set_email("name123@example.com".to_string());
/// ```
#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct PersonData {
    /// PersonData's id
    id: String,
    /// Phone number to use to contact this person
    pub phone: String,
    /// Email to use to contact this person
    pub email: String,
    /// Personal note
    pub note: String,
    /// Profesionnal social media
    pub social: Social,
}

impl PersonData {
    /// Create a new instance of `PersonData` with empty fields.
    /// Use builder pattern with [`set_phone`], [`set_email`], [`set_note`] and [`set_social`] methods.
    ///
    /// ### Example
    ///
    /// ```rust, ignore
    /// let data = PersonData::new()
    ///     .set_phone("0102030405".to_string());
    /// ```
    pub fn new() -> Self {
        Self {
            id: new_id(),
            phone: String::new(),
            email: String::new(),
            note: String::new(),
            social: Social::new(),
        }
    }

    /// Method to set the `phone` field. Return self to use Builder Pattern.
    pub fn set_phone(mut self, phone_number: String) -> Self {
        self.phone = phone_number;
        self
    }

    /// Method to set the `email` field. Return self to use Builder Pattern.
    pub fn set_email(mut self, email: String) -> Self {
        self.email = email;
        self
    }

    /// Method to set the `note` field. Return  self to use Builder Pattern.
    pub fn set_note(mut self, note: String) -> Self {
        self.note = note;
        self
    }

    /// Method to set the `social` field. Clone fields of `social` argument in the current `social` field
    ///  to don't change social's id. Return self to use Builder Pattern.
    pub fn set_social(mut self, social: &Social) -> Self {
        self.social.web_site = social.web_site.clone();
        self.social.linkedin = social.linkedin.clone();
        self.social.indeed = social.indeed.clone();
        self
    }
}

impl Identifiable for PersonData {
    fn get_id(&self) -> String {
        self.id.clone()
    }
}

impl PartialEq for PersonData {
    fn eq(&self, other: &Self) -> bool {
        self.get_id() == other.get_id()
    }

    fn ne(&self, other: &Self) -> bool {
        self.get_id() != other.get_id()
    }
}
