use serde::{Deserialize, Serialize};

use crate::{utils::new_id, Identifiable, PersonData};

/// Define a contact from a company.
#[derive(Debug, Deserialize, Serialize)]
pub struct Contact {
    /// contact's id
    id: String,
    /// Contact's name
    pub name: String,
    /// Company's id of this contact
    pub company_id: String,
    /// Information (phone number, mail, profesionnal social media) of this contact.
    pub information: PersonData,
}

impl Contact {
    /// Create a new contact, (generate a new id).
    pub fn new(name: String, company_id: String) -> Self {
        Self {
            id: new_id(),
            name,
            company_id,
            information: PersonData::new(),
        }
    }
}

impl Identifiable for Contact {
    fn get_id(&self) -> String {
        self.id.clone()
    }
}

impl PartialEq for Contact {
    fn eq(&self, other: &Self) -> bool {
        self.get_id() == other.get_id()
    }

    fn ne(&self, other: &Self) -> bool {
        self.get_id() != other.get_id()
    }
}
