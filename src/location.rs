use serde::{Deserialize, Serialize};

/// Represent a location, physical or virtual to job interview and location of company.
/// By default the location is [`Location::Unknow`].
#[cfg_attr(feature = "test", derive(PartialEq))]
#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(tag = "type")]
pub enum Location {
    /// Remote location with the url (optionaly)
    Remote { url: Option<String> },
    /// Real location
    InPlace {
        country: String,
        city: String,
        address: String,
    },
    /// Unknow location
    Unknown,
}

impl Default for Location {
    fn default() -> Self {
        Self::Unknown
    }
}
