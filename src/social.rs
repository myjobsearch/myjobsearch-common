use serde::{Deserialize, Serialize};

use crate::{utils::new_id, Identifiable};

/// Structure to group social data:
///
///  * Web site url
///  * Linkedin
///  * Indeed
///
/// ### Example
///
/// ```rust, ignore
/// let social = Social::new()
///     .set_web_site("https://www.company-web-site.com/".to_string())
///        .set_indeed("https://www.indeed.com/cmp/company-name".to_string())
///        .set_linkedin("https://www.linkedin.com/company/company-name".to_string());
/// ```
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Social {
    /// identifiant for database
    id: String,
    /// Web Site **Url**.
    pub web_site: String,
    /// Linkedin profile page **Url**.
    pub linkedin: String,
    /// Indeed profile page **Url**.
    pub indeed: String,
}

impl Social {
    /// Create a new instance of `Social` with empty fields.
    /// Use builder pattern with [`set_web_site`], [`set_linkedin`], [`set_indeed`] methods.
    ///
    /// ### Example
    ///
    /// ```rust, ignore
    /// let social = Sociel::new()
    ///     .set_web_site("https://company-web-site.com".to_string());
    /// ```
    pub fn new() -> Self {
        Self {
            id: new_id(),
            web_site: String::new(),
            linkedin: String::new(),
            indeed: String::new(),
        }
    }

    /// Method to set the `web_site` field. Return a reference to self to use Builder Pattern.
    pub fn set_web_site(mut self, url: String) -> Self {
        self.web_site = url;
        self
    }

    /// Method to set the `linkedin` field. Return a reference to self to use Builder Pattern.
    pub fn set_linkedin(mut self, url: String) -> Self {
        self.linkedin = url;
        self
    }

    /// Method to set the `indeed` field. Return a reference to self to use Builder Pattern.
    pub fn set_indeed(mut self, url: String) -> Self {
        self.indeed = url;
        self
    }
}

impl Identifiable for Social {
    fn get_id(&self) -> String {
        self.id.clone()
    }
}

impl PartialEq for Social {
    fn eq(&self, other: &Self) -> bool {
        self.get_id() == other.get_id()
    }

    fn ne(&self, other: &Self) -> bool {
        self.get_id() != other.get_id()
    }
}
