use chrono::{DateTime, Local};
use serde::{Deserialize, Serialize};

use crate::{
    utils::{date_format, new_id},
    Identifiable, JobInterview,
};

/// Define a job application
#[derive(Debug, Serialize, Deserialize)]
pub struct JobApplication {
    /// JobApplication's id
    id: String,
    /// Start date of application
    #[serde(with = "date_format")]
    start_date: DateTime<Local>,
    /// company's id for this application
    company_id: String,
    /// Job offer url of this application
    pub job_offer_url: String,
    /// Job interviews (upcoming, finished, or cancelled) for this application.
    pub job_interviews: Vec<JobInterview>,
}

impl JobApplication {
    /// Create a new application (with new id) from the company's id.
    pub fn new(company_id: String) -> Self {
        Self {
            company_id,
            id: new_id(),
            start_date: Local::now(),
            job_offer_url: String::new(),
            job_interviews: Vec::new(),
        }
    }

    /// Get company's id
    pub fn company_id(&self) -> String {
        self.company_id.clone()
    }

    /// Get the start date of application
    pub fn start_date(&self) -> &DateTime<Local> {
        &self.start_date
    }
}

impl Identifiable for JobApplication {
    fn get_id(&self) -> String {
        self.id.clone()
    }
}

impl PartialEq for JobApplication {
    fn eq(&self, other: &Self) -> bool {
        self.get_id() == other.get_id()
    }

    fn ne(&self, other: &Self) -> bool {
        self.get_id() != other.get_id()
    }
}
