use chrono::{DateTime, Local};
#[cfg(feature = "test")]
use chrono::{Datelike, Timelike};
use serde::{Deserialize, Serialize};

use crate::{
    utils::{date_format, new_id},
    Identifiable, Location,
};

/// Define the status of a job interview, this status can be:
///
///  * `Finished`: if the job interview is passed.
///  * `Upcoming`: if the job interview is not passed.
///  * `Cancelled`: if the job interview was cancelled.
#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub enum JobInterviewStatus {
    /// the job interview is passed
    #[serde(with = "date_format")]
    Finished(DateTime<Local>),
    /// the job interview is not passed
    #[serde(with = "date_format")]
    Upcoming(DateTime<Local>),
    /// the job interview was cancelled
    Cancelled,
}

#[cfg(feature = "test")]
impl PartialEq for JobInterviewStatus {
    fn eq(&self, other: &Self) -> bool {
        match self {
            Self::Cancelled => matches!(other, Self::Cancelled),
            Self::Finished(self_date) => {
                if let Self::Finished(other_date) = other {
                    self_date.year() == other_date.year()
                        && self_date.month() == other_date.month()
                        && self_date.day() == other_date.day()
                        && self_date.hour() == other_date.hour()
                        && self_date.minute() == other_date.minute()
                } else {
                    false
                }
            }
            Self::Upcoming(self_date) => {
                if let Self::Upcoming(other_date) = other {
                    self_date.year() == other_date.year()
                        && self_date.month() == other_date.month()
                        && self_date.day() == other_date.day()
                        && self_date.hour() == other_date.hour()
                        && self_date.minute() == other_date.minute()
                } else {
                    false
                }
            }
        }
    }

    fn ne(&self, other: &Self) -> bool {
        match self {
            Self::Cancelled => !matches!(other, Self::Cancelled),
            Self::Finished(self_date) => {
                if let Self::Finished(other_date) = other {
                    self_date.year() != other_date.year()
                        || self_date.month() != other_date.month()
                        || self_date.day() != other_date.day()
                        || self_date.hour() != other_date.hour()
                        || self_date.minute() != other_date.minute()
                } else {
                    true
                }
            }
            Self::Upcoming(self_date) => {
                if let Self::Upcoming(other_date) = other {
                    self_date.year() != other_date.year()
                        || self_date.month() != other_date.month()
                        || self_date.day() != other_date.day()
                        || self_date.hour() != other_date.hour()
                        || self_date.minute() != other_date.minute()
                } else {
                    true
                }
            }
        }
    }
}

/// Define a job interview
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct JobInterview {
    /// Job interview's id
    id: String,
    /// Job interview's status, contain the date of the job interview (if not cancelled).
    pub status: JobInterviewStatus,
    /// Location of the job interview.
    pub location: Location,
}

impl JobInterview {
    /// Create a new job interview
    pub fn new(datetime: DateTime<Local>, location: Location) -> Self {
        Self {
            id: new_id(),
            status: JobInterviewStatus::Upcoming(datetime),
            location,
        }
    }
}

impl Identifiable for JobInterview {
    fn get_id(&self) -> String {
        self.id.clone()
    }
}

impl PartialEq for JobInterview {
    fn eq(&self, other: &Self) -> bool {
        self.get_id() == other.get_id()
    }

    fn ne(&self, other: &Self) -> bool {
        self.get_id() != other.get_id()
    }
}
