use chrono::{DateTime, Local};
use serde::{Deserialize, Serialize};

use crate::{
    utils::{date_format, new_id, optional_date_format},
    GlobalScore, Identifiable, Location, Social,
};

/// Structure to define a company.
#[derive(Debug, Serialize, Deserialize)]
pub struct Company {
    /// Company's id
    id: String,
    /// Name of the company.
    pub name: String,
    /// Short description of the company.
    pub description: String,
    /// Location of the company.
    pub location: Location,
    /// Field of activities of the company.
    pub activity_fields: Vec<String>,
    /// Size (number of employees) of the company.
    pub size: u16,
    /// Turnover of the company
    pub turnover: f64,
    /// Personal score/rating for this company. Value between 0 and 100.
    pub score: GlobalScore,
    /// Date when the company was added (in application/database).
    #[serde(with = "date_format")]
    date_add: DateTime<Local>,
    /// Date of the last contact with company.
    #[serde(with = "optional_date_format")]
    pub date_last_contact: Option<DateTime<Local>>,
    /// Social data of the company.
    pub social: Social,
}

impl Company {
    /// Create a new company. Set the current date & time for `date_add` field.
    pub fn new(name: String) -> Self {
        Self {
            name,
            id: new_id(),
            description: String::new(),
            activity_fields: Vec::new(),
            location: Location::default(),
            size: 0,
            turnover: 0f64,
            score: GlobalScore::new(),
            date_add: Local::now(),
            date_last_contact: None,
            social: Social::new(),
        }
    }

    /// Get the date add of the company
    pub fn date_add(&self) -> &DateTime<Local> {
        &self.date_add
    }
}

impl Identifiable for Company {
    fn get_id(&self) -> String {
        self.id.clone()
    }
}

impl PartialEq for Company {
    fn eq(&self, other: &Self) -> bool {
        self.get_id() == other.get_id()
    }

    fn ne(&self, other: &Self) -> bool {
        self.get_id() != other.get_id()
    }
}
