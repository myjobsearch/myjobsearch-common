use nanoid::nanoid;

/// Date format use to serialize and deserialize `DateTime<Local>`
pub(crate) const DATETIME_FORMAT: &'static str = "%Y-%m-%d %H:%M";

/// Function to create a new identifiant based on nanoid.
pub fn new_id() -> String {
    nanoid!(30)
}

pub(crate) mod date_format {
    use super::DATETIME_FORMAT;
    use chrono::{DateTime, Local, TimeZone};
    use serde::{Deserialize, Deserializer, Serializer};

    pub fn serialize<S>(date: &DateTime<Local>, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let s = format!("{}", date.format(DATETIME_FORMAT));
        serializer.serialize_str(&s)
    }

    pub fn deserialize<'de, D>(deserializer: D) -> Result<DateTime<Local>, D::Error>
    where
        D: Deserializer<'de>,
    {
        let s = String::deserialize(deserializer)?;
        Local
            .datetime_from_str(&s, DATETIME_FORMAT)
            .map_err(serde::de::Error::custom)
    }
}

pub(crate) mod optional_date_format {
    use super::DATETIME_FORMAT;
    use chrono::{DateTime, Local, TimeZone};
    use serde::{Deserialize, Deserializer, Serializer};

    pub fn serialize<S>(
        option_date: &Option<DateTime<Local>>,
        serializer: S,
    ) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        match option_date {
            Some(date) => serializer.serialize_str(&format!("{}", date.format(DATETIME_FORMAT))),
            None => serializer.serialize_none(),
        }
    }

    pub fn deserialize<'de, D>(deserializer: D) -> Result<Option<DateTime<Local>>, D::Error>
    where
        D: Deserializer<'de>,
    {
        let opt_string: Option<String> = Option::deserialize(deserializer)?;
        match opt_string {
            Some(s) => Ok(Some(
                Local
                    .datetime_from_str(&s, DATETIME_FORMAT)
                    .map_err(serde::de::Error::custom)?,
            )),
            None => Ok(None),
        }
    }
}
