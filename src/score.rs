//! Module to define all related score structures and functions.
//!
//! The score system is customizable. It is comosed of one or more criteria with a coefficient define by user.
pub use serde::{Deserialize, Serialize};

use crate::{utils::new_id, Identifiable};

/// Define a criteria, with the name, a description and the coefficient.
#[derive(Debug, Deserialize, Serialize)]
pub struct Criteria {
    id: String,
    /// Criteria's name
    pub name: String,
    /// Criteria's coeffcient
    pub coefficient: u8,
    /// Criteria's description
    pub description: String,
}

impl Criteria {
    /// Create a new criteria instance from a name. By default, description is empty and coefficient is 1. To customize, use `set_description` and `set_coeff` methods.
    pub fn new(name: String) -> Self {
        Self {
            /// Criteria's name
            name,
            /// Criteria's id
            id: new_id(),
            /// Criteria's description
            description: String::new(),
            /// Coefficient for this criteria
            coefficient: 1,
        }
    }

    /// Method to set the `description` field. Return self to use Builder Pattern.
    pub fn set_description(mut self, description: String) -> Self {
        self.description = description;
        self
    }

    /// Method to set the `coefficient` field. Return self to use Builder Pattern.
    pub fn set_coeff(mut self, coeff: u8) -> Self {
        self.coefficient = coeff;
        self
    }
}

impl Identifiable for Criteria {
    fn get_id(&self) -> String {
        self.id.clone()
    }
}

impl PartialEq for Criteria {
    fn eq(&self, other: &Self) -> bool {
        self.get_id() == other.get_id()
    }

    fn ne(&self, other: &Self) -> bool {
        self.get_id() != other.get_id()
    }
}

/// Define a score for a specific criteria.
#[derive(Debug, Deserialize, Serialize)]
pub struct CriteriaScore {
    /// Criteria's id
    criteria_id: String,
    /// Score for this criteria, between 0 and 100.
    pub score: u8,
}

impl CriteriaScore {
    /// Create a new criteria's score
    pub fn new(criteria_id: String, score: u8) -> Self {
        Self { score, criteria_id }
    }

    /// Get criteria's id
    pub fn get_criteria_id(&self) -> String {
        self.criteria_id.clone()
    }

    /// Create a new criteria's score from a [`Criteria`]
    pub fn from_criteria(criteria: &Criteria, score: u8) -> Self {
        Self::new(criteria.get_id(), score)
    }
}

impl PartialEq for CriteriaScore {
    fn eq(&self, other: &Self) -> bool {
        self.get_criteria_id() == other.get_criteria_id() && self.score == other.score
    }

    fn ne(&self, other: &Self) -> bool {
        self.get_criteria_id() != other.get_criteria_id() || self.score != other.score
    }
}

impl Eq for CriteriaScore {}

/// Define the personal score for a company.
#[derive(Debug, Deserialize, Serialize)]
pub struct GlobalScore {
    /// GlobalScore's id
    id: String,
    /// Specific criteria's score
    criterias: Vec<CriteriaScore>,
}

impl GlobalScore {
    /// Create a new global score (with a new id)
    pub fn new() -> Self {
        Self {
            id: new_id(),
            criterias: Vec::new(),
        }
    }

    /// Add a criteria score to the global score
    pub fn add_criteria(&mut self, criteria: CriteriaScore) {
        self.criterias.push(criteria);
    }

    /// Returns the size of `criterias` field
    pub fn get_criteria_number(&self) -> usize {
        self.criterias.len()
    }

    /// Compute the global score.
    pub fn score(&self, model_criterias: &Vec<Criteria>) -> u8 {
        let mut sum_score: u16 = 0;
        let mut sum_coeff: u8 = 0;
        for criteria in self.criterias.iter() {
            if let Some(model) = model_criterias
                .iter()
                .find(|model| model.get_id() == criteria.get_criteria_id())
            {
                sum_coeff += model.coefficient;
                sum_score += (model.coefficient * criteria.score) as u16;
            }
        }
        if sum_coeff == 0 {
            0
        } else {
            (sum_score / sum_coeff as u16) as u8
        }
    }
}

impl Identifiable for GlobalScore {
    fn get_id(&self) -> String {
        self.id.clone()
    }
}

impl PartialEq for GlobalScore {
    fn eq(&self, other: &Self) -> bool {
        self.get_id() == other.get_id()
    }

    fn ne(&self, other: &Self) -> bool {
        self.get_id() != other.get_id()
    }
}
