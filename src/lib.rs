//! A crate to group common structures, traits, function to use in different application for  [MyJobSearch][group-repo] project.
//!
//! All structures can be serialized and deserialized with [serde](https://crates.io/crates/serde).
//!
//! [group-repo]: https://gitlab.com/myjobsearch

mod company;
mod contact;
mod job_application;
mod job_interview;
mod location;
mod person_data;
mod score;
mod social;
mod utils;

pub use company::Company;
pub use contact::Contact;
pub use job_application::JobApplication;
pub use job_interview::{JobInterview, JobInterviewStatus};
pub use location::Location;
pub use person_data::PersonData;
pub use score::{Criteria, CriteriaScore, GlobalScore};
pub use social::Social;

pub trait Identifiable {
    fn get_id(&self) -> String;
}
