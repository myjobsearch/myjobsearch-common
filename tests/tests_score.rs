#[cfg(test)]
mod tests_score {
    use myjobsearch_common::{Criteria, CriteriaScore, GlobalScore, Identifiable};
    use serde_json;

    #[test]
    fn test_criteria() {
        let criteria = Criteria::new("Feature".to_string())
            .set_description("Some feature".to_string())
            .set_coeff(2);

        assert_eq!(&criteria.name, "Feature");
        assert_eq!(&criteria.description, "Some feature");
        assert_eq!(criteria.coefficient, 2);
    }

    #[test]
    fn test_criteria_score() {
        let criteria = Criteria::new("Feature".to_string()).set_coeff(2);
        let criteria_score = CriteriaScore::from_criteria(&criteria, 80);
        assert_eq!(criteria_score.get_criteria_id(), criteria.get_id());
        assert_eq!(criteria_score.score, 80);
    }

    #[test]
    fn test_globalscore() {
        let criteria1 = Criteria::new("Feature 1".to_string()).set_coeff(2);
        let criteria2 = Criteria::new("Feature 2".to_string());
        let mut global_score = GlobalScore::new();
        global_score.add_criteria(CriteriaScore::from_criteria(&criteria1, 60));
        global_score.add_criteria(CriteriaScore::from_criteria(&criteria2, 80));

        assert_eq!(global_score.get_criteria_number(), 2);
        assert_eq!(global_score.score(&Vec::new()), 0);
        assert_eq!(global_score.score(&(vec![criteria1, criteria2])), 66);
    }

    #[test]
    fn test_criteria_serialization() {
        let criteria = Criteria::new("Feature".to_string()).set_coeff(3);
        let criteria_to_deserialize = format!(
            "{{\
            \"id\":\"{id}\",\
            \"name\":\"Feature\",\
            \"coefficient\":3,\
            \"description\":\"\"\
        }}",
            id = criteria.get_id()
        );

        let criteria_serialized = serde_json::to_string(&criteria).unwrap();
        let criteria_deserialized: Criteria =
            serde_json::from_str(&criteria_to_deserialize).unwrap();
        assert_eq!(criteria_serialized, criteria_to_deserialize);
        assert_eq!(criteria_deserialized, criteria);
    }

    #[test]
    fn test_criteria_score_serialization() {
        let criteria = Criteria::new("Feature".to_string()).set_coeff(2);
        let criteria_score = CriteriaScore::from_criteria(&criteria, 80);
        let criteria_score_to_deserialize = format!(
            "{{\
            \"criteria_id\":\"{id}\",\
            \"score\":80\
        }}",
            id = criteria.get_id()
        );

        let criteria_score_serialized = serde_json::to_string(&criteria_score).unwrap();
        let criteria_score_deserialized: CriteriaScore =
            serde_json::from_str(&criteria_score_to_deserialize).unwrap();

        assert_eq!(criteria_score_serialized, criteria_score_to_deserialize);
        assert_eq!(criteria_score_deserialized, criteria_score);
    }

    #[test]
    fn test_globalscore_serialization() {
        let mut global_score = GlobalScore::new();
        global_score.add_criteria(CriteriaScore::new("criteria-id".to_string(), 60));

        let globalscore_to_deserialize = format!(
            "{{\
            \"id\":\"{id}\",\
            \"criterias\":[{{\
                \"criteria_id\":\"criteria-id\",\
                \"score\":60\
            }}]\
        }}",
            id = global_score.get_id()
        );

        let global_score_serialized = serde_json::to_string(&global_score).unwrap();
        let global_score_deserialized: GlobalScore =
            serde_json::from_str(&globalscore_to_deserialize).unwrap();
        assert_eq!(global_score_serialized, globalscore_to_deserialize);
        assert_eq!(global_score_deserialized, global_score);
    }
}
