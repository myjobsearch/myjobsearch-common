#[cfg(test)]
mod tests_contact {
    use myjobsearch_common::{Contact, Identifiable};
    use serde_json;

    #[test]
    fn test_contact_serialization() {
        let contact = Contact::new("Someone".to_string(), "id01".to_string());
        let str_to_serialized = format!(
            "{{\
            \"id\":\"{}\",\
            \"name\":\"Someone\",\
            \"company_id\":\"id01\",\
            \"information\":{{\
                \"id\":\"{}\",\
                \"phone\":\"\",\
                \"email\":\"\",\
                \"note\":\"\",\
                \"social\":{{\
                    \"id\":\"{}\",\
                    \"web_site\":\"\",\
                    \"linkedin\":\"\",\
                    \"indeed\":\"\"\
                }}\
            }}\
        }}",
            contact.get_id(),
            contact.information.get_id(),
            contact.information.social.get_id()
        );

        let deserialized: Contact = serde_json::from_str(&str_to_serialized).unwrap();
        let serialized = serde_json::to_string(&contact).unwrap();
        assert_eq!(deserialized, contact);
        assert_eq!(serialized, str_to_serialized);
    }
}
