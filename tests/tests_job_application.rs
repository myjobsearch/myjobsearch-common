#[cfg(test)]
mod tests_job_application {
    use chrono::{Datelike, Duration, Local, Timelike};
    use myjobsearch_common::{
        Identifiable, JobApplication, JobInterview, JobInterviewStatus, Location,
    };
    use serde_json;

    #[test]
    fn test_job_interview_status() {
        let now = Local::now();
        let later_date = now + Duration::days(5);
        let finished_status = JobInterviewStatus::Finished(now.clone());
        let upcoming_status = JobInterviewStatus::Upcoming(later_date.clone());
        let cancelled_status = JobInterviewStatus::Cancelled;

        // Serialize DateTime en Unix timestamp (entier) ?
        let finished_to_deserialize = format!(
            "{{\"Finished\":\"{year}-{month:02}-{day:02} {hour:02}:{minutes:02}\"}}",
            year = now.year(),
            month = now.month(),
            day = now.day(),
            hour = now.hour(),
            minutes = now.minute()
        );
        let upcoming_to_deserialize = format!(
            "{{\"Upcoming\":\"{year}-{month:02}-{day:02} {hour:02}:{minutes:02}\"}}",
            year = later_date.year(),
            month = later_date.month(),
            day = later_date.day(),
            hour = later_date.hour(),
            minutes = later_date.minute()
        );
        let cancelled_to_deserialize = "\"Cancelled\"".to_string();

        let finished_serialized = serde_json::to_string(&finished_status).unwrap();
        let upcoming_serialized = serde_json::to_string(&upcoming_status).unwrap();
        let cancelled_serialized = serde_json::to_string(&cancelled_status).unwrap();
        let finished_deserialized: JobInterviewStatus =
            serde_json::from_str(&finished_to_deserialize).unwrap();
        let upcoming_deserialized: JobInterviewStatus =
            serde_json::from_str(&upcoming_to_deserialize).unwrap();
        let cancelled_deserialized: JobInterviewStatus =
            serde_json::from_str(&cancelled_to_deserialize).unwrap();

        assert_eq!(finished_serialized, finished_to_deserialize);
        assert_eq!(upcoming_serialized, upcoming_to_deserialize);
        assert_eq!(cancelled_serialized, cancelled_to_deserialize);
        assert_eq!(finished_deserialized, finished_status);
        assert_eq!(upcoming_deserialized, upcoming_status,);
        assert_eq!(cancelled_deserialized, cancelled_status);
    }

    #[test]
    fn test_job_interview_serialization() {
        let now = Local::now();
        let job_interview = JobInterview::new(now, Location::Unknown);
        let str_to_deserialize = format!(
            "{{\
            \"id\":\"{id}\",\
            \"status\":{{\"Upcoming\":\"{year}-{month:02}-{day:02} {hour:02}:{minutes:02}\"}},\
            \"location\":{{\"type\":\"Unknown\"}}\
        }}",
            id = job_interview.get_id(),
            year = now.year(),
            month = now.month(),
            day = now.day(),
            hour = now.hour(),
            minutes = now.minute()
        );

        let serialized = serde_json::to_string(&job_interview).unwrap();
        let deserialized: JobInterview = serde_json::from_str(&str_to_deserialize).unwrap();
        assert_eq!(serialized, str_to_deserialize);
        assert_eq!(deserialized, job_interview);
    }

    #[test]
    fn test_job_application_serialization() {
        let now = Local::now();
        let mut job_application = JobApplication::new("company-id".to_string());
        job_application.job_offer_url = "https://company.com/my-job-offer/".to_string();
        let str_to_deserialize = format!(
            "{{\
            \"id\":\"{id}\",\
            \"start_date\":\"{year}-{month:02}-{day:02} {hour:02}:{minute:02}\",\
            \"company_id\":\"company-id\",\
            \"job_offer_url\":\"{url}\",\
            \"job_interviews\":[]\
        }}",
            id = job_application.get_id(),
            year = now.year(),
            month = now.month(),
            day = now.day(),
            hour = now.hour(),
            minute = now.minute(),
            url = &job_application.job_offer_url
        );

        let serialized = serde_json::to_string(&job_application).unwrap();
        let deserialized: JobApplication = serde_json::from_str(&str_to_deserialize).unwrap();
        assert_eq!(serialized, str_to_deserialize);
        assert_eq!(deserialized, job_application);
    }
}
