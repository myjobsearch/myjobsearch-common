#[cfg(test)]
mod tests_company {
    use chrono::{Datelike, Timelike};
    use myjobsearch_common::{Company, Identifiable};
    use serde_json;

    #[test]
    fn test_company_serialization() {
        let mut company = Company::new("Some".to_string());
        company.turnover = 300f64;
        company.social.web_site = "https://some.com".to_string();
        company.size = 20;

        let company_to_deserialize = format!(
            "{{\
                \"id\":\"{id}\",\
                \"name\":\"Some\",\
                \"description\":\"\",\
                \"location\":{{\"type\":\"Unknown\"}},\
                \"activity_fields\":[],\
                \"size\":20,\
                \"turnover\":300.0,\
                \"score\":{{\
                    \"id\":\"{score_id}\",\
                    \"criterias\":[]\
                }},\
                \"date_add\":\"{year}-{month:02}-{day:02} {hour:02}:{minute:02}\",\
                \"date_last_contact\":null,\
                \"social\":{{\
                    \"id\":\"{social_id}\",\
                    \"web_site\":\"https://some.com\",\
                    \"linkedin\":\"\",\
                    \"indeed\":\"\"\
                }}\
            }}",
            id = company.get_id(),
            score_id = company.score.get_id(),
            social_id = company.social.get_id(),
            year = company.date_add().year(),
            month = company.date_add().month(),
            day = company.date_add().day(),
            hour = company.date_add().hour(),
            minute = company.date_add().minute()
        );

        let company_serialized = serde_json::to_string(&company).unwrap();
        let company_deserialized: Company = serde_json::from_str(&company_to_deserialize).unwrap();
        assert_eq!(company_serialized, company_to_deserialize);
        assert_eq!(company_deserialized, company);
    }
}
