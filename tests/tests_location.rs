#[cfg(test)]
mod tests_location {
    use myjobsearch_common::Location;
    use serde_json;

    #[test]
    fn test_remote_location() {
        let location = Location::Remote {
            url: Some("https://example.com".to_string()),
        };

        let str_to_serialize = "{\"type\":\"Remote\",\"url\":\"https://example.com\"}";
        let deserialized: Location = serde_json::from_str(str_to_serialize).unwrap();
        let serialized = serde_json::to_string(&location).unwrap();
        assert_eq!(deserialized, location);
        assert_eq!(serialized, str_to_serialize);
    }

    #[test]
    fn test_inplace_location() {
        let location = Location::InPlace {
            country: "Bed".to_string(),
            city: "DreamLand".to_string(),
            address: "".to_string(),
        };
        let str_to_serialize = "\
{\
    \"type\":\"InPlace\",\
    \"country\":\"Bed\",\
    \"city\":\"DreamLand\",\
    \"address\":\"\"\
}";
        let deserialized: Location = serde_json::from_str(str_to_serialize).unwrap();
        let serialized = serde_json::to_string(&location).unwrap();
        assert_eq!(deserialized, location);
        assert_eq!(serialized, str_to_serialize);
    }

    #[test]
    fn test_unknow_location() {
        let location = Location::Unknown;

        let str_to_serialize = "{\"type\":\"Unknown\"}";
        let deserialized: Location = serde_json::from_str(str_to_serialize).unwrap();
        let serialized = serde_json::to_string(&location).unwrap();
        assert_eq!(deserialized, location);
        assert_eq!(serialized, str_to_serialize);
    }
}
