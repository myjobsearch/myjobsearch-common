#[cfg(test)]
mod tests_person_data {
    use myjobsearch_common::{Identifiable, PersonData, Social};
    use serde_json;

    #[test]
    fn test_person_data_serialization() {
        let data = PersonData::new()
            .set_email("name@example.com".to_string())
            .set_phone("0102030405".to_string());

        let str_to_serialized = format!(
            "{{\
            \"id\":\"{}\",\
            \"phone\":\"0102030405\",\
            \"email\":\"name@example.com\",\
            \"note\":\"\",\
            \"social\":{{\
                \"id\":\"{}\",\
                \"web_site\":\"\",\
                \"linkedin\":\"\",\
                \"indeed\":\"\"\
            }}\
        }}",
            data.get_id(),
            data.social.get_id()
        );

        let deserialized: PersonData = serde_json::from_str(&str_to_serialized).unwrap();
        let serialized = serde_json::to_string(&data).unwrap();
        assert_eq!(deserialized, data);
        assert_eq!(serialized, str_to_serialized);
    }

    #[test]
    fn test_person_data_builder() {
        let data = PersonData::new()
            .set_email("name@example.com".to_string())
            .set_note("Some note".to_string())
            .set_phone("0102030405".to_string())
            .set_social(&Social::new().set_web_site("https://my-website.com".to_string()));
        assert_eq!(data.email, "name@example.com");
        assert_eq!(data.note, "Some note");
        assert_eq!(data.phone, "0102030405");
        assert_eq!(data.social.web_site, "https://my-website.com");
    }
}
