#[cfg(test)]
mod tests_social {
    use myjobsearch_common::{Identifiable, Social};
    use serde_json;

    #[test]
    fn test_social_serialization() {
        let social = Social::new().set_web_site("https://my-website.com".to_string());

        let str_to_serialize = format!(
            "{{\
            \"id\":\"{}\",\
            \"web_site\":\"https://my-website.com\",\
            \"linkedin\":\"\",\
            \"indeed\":\"\"\
        }}",
            social.get_id()
        );

        let deserialized: Social = serde_json::from_str(&str_to_serialize).unwrap();
        let serialized = serde_json::to_string(&social).unwrap();
        assert_eq!(deserialized, social);
        assert_eq!(serialized, str_to_serialize);
    }

    #[test]
    fn test_social_builder() {
        let social = Social::new()
            .set_web_site("https://my-website.com".to_string())
            .set_linkedin("https://my-linkedin.com".to_string())
            .set_indeed("https://my-indeed.com".to_string());
        assert_eq!(social.web_site, "https://my-website.com");
        assert_eq!(social.linkedin, "https://my-linkedin.com");
        assert_eq!(social.indeed, "https://my-indeed.com");
    }
}
